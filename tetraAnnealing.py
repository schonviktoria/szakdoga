#!/usr/bin/python
''' 
Implementation: Stelios Babrerakis, 
                 email: chefarov@gmail.com
                 copyleft May 2012
                 
'''          

import os
import sys
import time
import math
import random
import re
import numpy as np
from scipy import optimize

def fileLength(filename):
    with open(filename, "r") as f:
	    # n definition
        pos = f.tell()
        line = f.readline()
        f.seek(pos)
        line = line.rstrip()
        tmp = line.split(' ')
        n = len(tmp)
        print("n= ", n)
        # m definition
        m = 0
        for line in f:
            if re.match('\n', line):
                m = m+1
        print("m= ", m)
        f.seek(pos)
    return n, m

def readFile(filename):
    n, m = fileLength(filename)
    board = -1 * np.ones((n, m, 4), dtype=np.int64) # TODO: add tile id to coordinate 5
    # the np.ones() fill the matrix with ones 
    # n * (m row, 4 coloumn)
    with open(filename, "r") as f:	    
        for i in range(n):
            # we read each line and then loop for every line in its words containing
            # one number each one. We need 3 lines for every tile (check input.txt)
            line = f.readline()
            line = line.rstrip(' \t\n\r')
            words = line.split(' ')
            for j in range(m):
                board[i, j, 0] = words[j][2] # up
            # next line
            line = f.readline()
            line = line.rstrip(' \t\n\r')
            words = line.split(' ')
            for j in range(m):
                board[i, j, 1] = words[j][0] # left
                board[i, j, 3] = words[j][4] # right
            # next line
            line = f.readline()
            line = line.rstrip(' \t\n\r')
            words = line.split(' ')
            for j in range(m):
                board[i, j, 2] = words[j][2] # down
            f.readline() #there is a blank line after each whole tile in input files
    return board

def printTiles(board):
    n, m, _ = np.shape(board)
    print('\n***********************')
    for i in range(n):
        for j in range(m):
            print("--" + str(board[i, j, 0]) + "-- ")
        print('\n')
        for j in range(m):
            print(str(board[i, j, 1]) + "---" + str(board[i, j, 3]) + " ")
        print('\n')
        for j in range(m):
            print("--" + str(board[i, j, 2]) + "-- ")
        print('\n----------------------')
    print('**********************')
    
def writeToFile(filename, board):
    n, m, _ = np.shape(board)
    with open(filename, "w") as f:
        for i in range(n):
            for j in range(m):
                f.write("--" + str(board[i, j, 0]) + "-- ")
            f.write('\n')
            for j in range(m):
                f.write(str(board[i, j, 1]) + "---" + str(board[i, j, 3]) + " ")
            f.write('\n')
            for j in range(m):
                f.write("--" + str(board[i, j, 2]) + "-- ")
            f.write('\n\n')
   
def evaluate(board): # return the count of conflicts
    n, m, _ = np.shape(board)
    conflicts = 0
    for i in range(n - 1):
        for j in range(m):
            if board[i, j, 2] != board[i + 1, j, 0]: # row i bottom != row i+1 top
                conflicts = conflicts + 1
    for i in range(n):
        for j in range(m - 1):
            if board[i, j, 3] != board[i, j + 1, 1]: # col j right != col j+1 left
                conflicts = conflicts + 1
    return conflicts

def swap(board, i1, j1, i2, j2): # swap 2 tile on the board
    swapped_board = board.copy()
    swapped_board[i1, j1, :] = board[i2, j2, :]
    swapped_board[i2, j2, :] = board[i1, j1, :]
    return swapped_board

def rotate(board, i, j, k):
    rotated_board = board.copy()
    rotated_board[i, j, :] = np.roll(board[i, j, :], k)
    return rotated_board

def expand(board, i, j):
    n, m, _ = np.shape(board)
    neighbors = []
    if i > 0: # not first row
        neighbors.append((i - 1, j))
        # neighbors.append(board[i - 1, j, :])
    if i < n - 1: # not last row
        neighbors.append((i + 1, j))
        # neighbors.append(board[i + 1, j, :])
    if j > 0: # not first col
        neighbors.append((i, j - 1))
        # neighbors.append(board[i, j - 1, :])
    if j < m - 1: # not last col
        neighbors.append((i, j + 1))
        # neighbors.append(board[i, j + 1, :])
    return neighbors
    
def move(board): # TODO: not just neighbors; rotate
    n, m, _ = np.shape(board)
    i1, j1 = np.random.randint(n), np.random.randint(m)
    if np.random.randint(2) == 0:
        #neighbors = expand(board, i1, j1) # get all neighbors tiles
        #i2, j2 = neighbors[np.random.randint(len(neighbors))]
        while True:
            i2, j2 = np.random.randint(n), np.random.randint(m)
            if i1 != i2 or j1 != j2:
                break
        moved_board = swap(board, i1, j1, i2, j2)
    else:
        moved_board = rotate(board, i1, j1, 1)
        # moved_board = rotate(moved_board, i1, j1, 1)
        # moved_board = rotate(moved_board, i2, j2, 1)
    return moved_board
    
def annealing(board, maxSteps=500):
    best_board = board.copy()
    prev_board = board.copy()
    steps = 0
    temperature = 100.0
    finalTemp = 0.1 # proper to get final probability small enough (less than 0.001)
    coolingFactor = float((finalTemp / temperature) ** (1.0 / float(maxSteps))) #normalizing temperature timeline to number of steps
    while steps < maxSteps:
        steps = steps + 1
        temperature = temperature * coolingFactor     
        new_board = move(prev_board)
        new_val = evaluate(new_board) 
        if new_val == 0: # found a solution without conflicts
            print('Step ' + str(steps) + ':')
            # printTiles(new_board)
            print('Conflicts: ' + str(0))
            return new_board, 0
        delta = new_val - evaluate(prev_board) # finding DE of new state against current.
        if delta < 0: # If new value is better (smaller) than current one
            prev_board = new_board # then we make the move for sure
        else: # If new value is bigger (worse) than current one
            probability = math.exp(-delta / temperature) # get probability to make the move
            if probability > np.random.rand():
                prev_board = new_board # make the move
        if np.mod(steps, 1000) == 1:
            print('Step ' + str(steps) + ':')
            # printTiles(prev_board)
            print('Conflicts: ' + str(evaluate(prev_board)))
        if new_val < evaluate(best_board): # If new value is better (smaller) than current one
            best_board = new_board # then we make the move for sure
    printTiles(best_board)
    print('Conflicts: ' + str(evaluate(best_board)))
    return best_board, evaluate(best_board)

def mix(board):
    n, m, _ = np.shape(board)
    rp = np.random.permutation(n * m)
    mixed_board = -1 * np.ones_like(board)
    for i in range(n):
        for j in range(m):
            k = rp[i * m + j]
            i2, j2 = k // m, np.mod(k, m)
            mixed_board[i, j, :] = board[i2, j2, :]
            l = np.random.randint(4)
            mixed_board = rotate(mixed_board, i, j, l)
    return mixed_board

def generate_solved(n, m, k):
    solved_board = -1 * np.ones((n, m, 4), dtype=np.int64) # TODO: add tile id to coordinate 5
    # np.ones : create m np.array nx4 dim , one array represents a tile
    for i in range(n - 1):
        for j in range(m):
            color = np.random.randint(1, k)
            solved_board[i, j, 2] = color
            solved_board[i + 1, j, 0] = color
    for i in range(n):
        for j in range(m - 1):
            color = np.random.randint(1, k)
            solved_board[i, j, 3] = color
            solved_board[i, j + 1, 1] = color
    for i in range(n):
        color = np.random.randint(1, k)
        solved_board[i, 0, 1] = color
        color = np.random.randint(1, k)
        solved_board[i, -1, 3] = color
    for j in range(m):
        color = np.random.randint(1, k)
        solved_board[0, j, 0] = color
        color = np.random.randint(1, k)
        solved_board[-1, j, 2] = color
    # frame must be 0
    for i in range(m): # first and last row
        color = 0
        solved_board[0, i, 0] = color
        solved_board[n-1, i, 2] = color
    for j in range(n): # left and right side
        color = 0
        solved_board[j, 0, 1] = color
        solved_board[j, m-1, 3] = color
    return solved_board

def main():
    solved_board = generate_solved(6, 8, 5)
    writeToFile('solved', solved_board)
    initial_board = mix(solved_board)
    filename = 'gen'
    writeToFile(filename, initial_board)

#    filename = input('Enter input file name:  ')
#    initial_board = readFile(filename)    

    np.random.seed(123)
    annealed_board, val = annealing(initial_board, 1000000) # optimize
    writeToFile(filename.split('.')[0] + '_result', annealed_board)

if __name__ == '__main__':
    main()

