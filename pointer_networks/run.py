from warnings import simplefilter
simplefilter(action='ignore', category=FutureWarning)
from keras.models import Model
from keras.layers import LSTM, Input, TimeDistributed, Dense, Bidirectional, Lambda
from keras import backend as K
from keras.utils.np_utils import to_categorical
from keras.optimizers import Adam
from keras.callbacks import LearningRateScheduler, TerminateOnNaN, ModelCheckpoint, EarlyStopping
import keras.engine.training
import pickle
import numpy as np
import EternityData as eternity
from tetraAnnealing import annealing, evaluate, annealing_2, evaluate_2
from joblib import dump, load
import argparse
from datetime import datetime
from termcolor import colored

''' ******************************************************************************
    Model0 = LSTM
    Model1 = Annealing after LSTM
    Model2 = Two different method annealing after LSTM
    Model3 = Only annealing
    ******************************************************************************'''

class Run:
    modelnum = 0
    rows = 2
    columns = 2
    colours = 4
    loadPrevModel = False
    random_value = 400
    trainCount = 10000
    earlyStopping = 20
    gamma=100
    hidden_size = 256
    nb_epochs = 200
    learning_rate = 0.1
    bidirectional = True
    saveModel = False

    X_val, Y_val = 0, 0
    X_test, Y_test = 0, 0
    X_train, Y_train = 0, 0

    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("--modelType", type=int, choices=[0, 1, 2, 3], help="set the model learning type")
        parser.add_argument("--rows", type=int, help="set the table rows size")
        parser.add_argument("--columns", type=int, help="set the table columns size")
        parser.add_argument("--colours", type=int, help="set the generated table colours size")
        parser.add_argument("--loadPrevModel", help="if exist param with string, we load prev saved model")
        parser.add_argument("--random", type=int, help="set the random value")
        parser.add_argument("-saveModel", help="flag for save model", action="store_true")
        parser.add_argument("--hiddenSize", type=int, choices=[128, 256, 512], help="set the hidden size")
        parser.add_argument("--epochs", type=int, help="set the training epochs")
        args = parser.parse_args()

        if args.modelType != None:
            self.modelnum = args.modelType

        if args.rows and args.rows <= 1:
            parser.error("Minimum rows size: 2")
        elif args.rows and args.rows >=2:
            self.rows = args.rows

        if args.columns and args.columns <= 1:
            parser.error("Minimum columns size: 2")
        elif args.columns and args.columns >=2:
            self.columns = args.columns

        if args.colours and args.colours <=1:
            parser.error("Minimum colours size: 2")
        elif args.colours and args.colours >= 2:
            self.colours = args.colours

        if args.loadPrevModel != None:
            self.loadPrevModel = args.loadPrevModel
        else:
            self.loadPrevModel = False

        if args.random:
            self.random_value = args.random

        if args.saveModel:
            self.saveModel = args.saveModel

        if args.hiddenSize:
            self.hiddenSize = args.hiddenSize

        if args.epochs:
            self.nb_epochs = args.epochs

        np.random.seed(self.random_value)

        self.data = eternity.EternityData(self.rows, self.columns, self.colours)

    def scheduler(epoch):
        if epoch < nb_epochs/4:
            return learning_rate
        elif epoch < nb_epochs/2:
            return learning_rate*0.5
        return learning_rate*0.1

    def generateData(self):
        self.X_val, self.Y_val = self.data.mixTables(self.data.generateSolvedTables(self.trainCount), self.modelnum)
        self.X_test, self.Y_test = self.data.mixTables(self.data.generateSolvedTables(self.trainCount), self.modelnum)
        self.X_train, self.Y_train = self.data.mixTables(self.data.generateSolvedTables(self.trainCount), self.modelnum)

    def learning(self):
        print("building model", self.modelnum, "bidirectional: ", self.bidirectional, self.rows, "x", self.columns, "color: ", self.colours, "hidden size: ", self.hidden_size, "...")
        now = datetime.now()
        dt = now.strftime("%Y-%m-%d_%H:%M:%S")
        filename = "model" + str(self.modelnum) + "_" + str(self.rows) + "x" + str(self.columns) + "colours" + str(self.colours)  + "_" + str(self.random_value) + "_" + str(dt) + ".txt"
        print("Writing to file: ", filename)
        self.data.setFileName(filename)

        if self.modelnum == 0:
            self.model_0()
        elif self.modelnum == 1:
            self.model_1()
        elif self.modelnum == 2:
            self.model_2()
        elif self.modelnum == 3:
            self.model_3()

    def model_0(self):
        main_input = Input(shape=(self.rows * self.columns * 4, self.colours), name='main_input')
        if self.bidirectional is True:
            fn = Bidirectional
        else:
            fn = lambda x: x
        encoder = fn(LSTM(units=self.hidden_size, return_sequences=True, name="encoder"))(main_input)
        encoder = fn(LSTM(units=self.hidden_size, return_sequences=True))(encoder)
        encoder = fn(LSTM(units=self.hidden_size, return_sequences=True))(encoder)
        decoder = TimeDistributed(Dense(units=self.colours, activation='softmax'))(encoder)
        model = Model(inputs=main_input, outputs=decoder)
        model.compile(optimizer=Adam(clipnorm=1.),
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])

        hdf5 = 'model_weight.hdf5'

        if self.loadPrevModel is not False:
            print("Mentett modell betöltése: " + self.loadPrevModel + ".h5")  
            try:                
                model.load_weights('../../saved_models/' + self.loadPrevModel + '.h5')
            except ValueError:
                print(colored('Nem megfelelő mentett modell fájlt választottál a megadott beállításokhoz.\nKérlek válassz másikat, vagy módosítsd a paramétereket a mentett modellhez.', 'red'))
                return
            except OSError:
                print(colored('Nincs ilyen nevű mentett fájl. Ellenőrizd a nevét, és a helyét.', 'red'))
                return
            model._make_train_function()
            with open('../../saved_models/' + self.loadPrevModel + '.pkl', 'rb') as f:
                try:
                    weight_values = pickle.load(f)
                except EOFError:
                    print(colored('Nem létező/üres pkl fájl.', 'red'))
                    return
            model.optimizer.set_weights(weight_values)
        else:
            print("Modell tanítása...")
            model.fit_generator(self.data.generator(batch_size=32, modelnum=self.modelnum), steps_per_epoch=100, epochs=self.nb_epochs, 
                                callbacks=[TerminateOnNaN(), 
                                ModelCheckpoint(hdf5, save_best_only=True), EarlyStopping(patience=self.earlyStopping)], 
                                validation_data=(self.X_val, self.Y_val))
            print(model.evaluate(self.X_test, self.Y_test, batch_size=32))

        if self.saveModel is True:
            model.save_weights('../../saved_models/' + str(self.rows) + 'x' + str(self.columns) + 'colours' + str(self.colours) + '.h5')
            symbolic_weights = getattr(model.optimizer, 'weights')
            weight_values = K.batch_get_value(symbolic_weights)
            with open('../../saved_models/' + str(self.rows) + 'x' + str(self.columns) + 'colours' + str(self.colours) + '.pkl', 'wb') as f:
                pickle.dump(weight_values, f)
            print('Modell mentése sikeresen befejeződött.')

        c = self.X_test.shape[0]
        self.data.writeArguments(int(c/100), self.rows, self.columns, self.colours)
        for i in range(0, c, 100):
            acc = evaluate(np.reshape(np.argmax(self.X_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)))
            acc2 = evaluate_2(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), np.reshape(np.argmax(self.X_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)))
            self.data.writeToFiles(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), 0, 0)
            self.data.writeToFiles(np.reshape(np.argmax(self.X_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), acc, acc2)
            acc2 = evaluate_2(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), np.reshape(np.argmax(model.predict(self.X_test[i:(i + 1)]), axis=-1), (self.rows, self.columns, 4)))
            acc = evaluate(np.reshape(np.argmax(model.predict(self.X_test[i:(i + 1)]), axis=-1), (self.rows, self.columns, 4)))
            self.data.writeToFiles(np.reshape(np.argmax(model.predict(self.X_test[i:(i + 1)]), axis=-1), (self.rows, self.columns, 4)), acc2, acc)
            i = i + self.rows*self.columns
        print(colored('Fájlba írás befejeződött.', 'green'))


    def model_1(self):
        main_input = Input(shape=(self.rows * self.columns * 4, self.colours), name='main_input')
        if self.bidirectional is True:
            fn = Bidirectional
        else:
            fn = lambda x: x
        encoder = fn(LSTM(units=self.hidden_size, return_sequences=True, name="encoder"))(main_input)
        encoder = fn(LSTM(units=self.hidden_size, return_sequences=True))(encoder)
        encoder = fn(LSTM(units=self.hidden_size, return_sequences=True))(encoder)
        decoder = TimeDistributed(Dense(units=self.colours, activation='softmax'))(encoder)
        model = Model(inputs=main_input, outputs=decoder)
        model.compile(optimizer=Adam(clipnorm=1.),
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])

        hdf5 = 'model_weight.hdf5'

        if self.loadPrevModel is not False:
            print("Mentett modell betöltése: " + self.loadPrevModel + ".h5")  
            try:                
                model.load_weights('../../saved_models/' + self.loadPrevModel + '.h5')
            except ValueError:
                print(colored('Nem megfelelő mentett modell fájlt választottál a megadott beállításokhoz.\nKérlek válassz másikat, vagy módosítsd a paramétereket a mentett modellhez.', 'red'))
                return
            except OSError:
                print(colored('Nincs ilyen nevű mentett fájl. Ellenőrizd a nevét, és a helyét.', 'red'))
                return
            model._make_train_function()
            with open('../../saved_models/' + self.loadPrevModel + '.pkl', 'rb') as f:
                weight_values = pickle.load(f)
            model.optimizer.set_weights(weight_values)

        else:
            print("Modell tanítása...")
            model.fit_generator(self.data.generator(batch_size=32, modelnum=self.modelnum), steps_per_epoch=100, epochs=self.nb_epochs, 
                                callbacks=[TerminateOnNaN(), 
                                ModelCheckpoint(hdf5, save_best_only=True), EarlyStopping(patience=self.earlyStopping)], 
                                validation_data=(self.X_val, self.Y_val))
            print(model.evaluate(self.X_test, self.Y_test, batch_size=32))

        if self.saveModel is True:
            model.save_weights('../../saved_models/' + str(self.rows) + 'x' + str(self.columns) + 'colours' + str(self.colours) + '.h5')
            symbolic_weights = getattr(model.optimizer, 'weights')
            weight_values = K.batch_get_value(symbolic_weights)
            with open('../../saved_models/' + str(self.rows) + 'x' + str(self.columns) + 'colours' + str(self.colours) + '.pkl', 'wb') as f:
                pickle.dump(weight_values, f)
            print('Modell mentése sikeresen befejeződött.')

        c = self.X_test.shape[0]
        self.data.writeArguments(int(c/100), self.rows, self.columns, self.colours)
        for i in range(0, c, 100):
            acc = evaluate(np.reshape(np.argmax(self.X_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)))
            acc2 = evaluate_2(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), np.reshape(np.argmax(self.X_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)))
            self.data.writeToFiles(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), 0, 0)
            self.data.writeToFiles(np.reshape(np.argmax(self.X_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), acc, acc2)
            acc2 = evaluate_2(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), np.reshape(np.argmax(model.predict(self.X_test[i:(i + 1)]), axis=-1), (self.rows, self.columns, 4)))
            acc = evaluate(np.reshape(np.argmax(model.predict(self.X_test[i:(i + 1)]), axis=-1), (self.rows, self.columns, 4)))
            self.data.writeToFiles(np.reshape(np.argmax(model.predict(self.X_test[i:(i + 1)]), axis=-1), (self.rows, self.columns, 4)), acc2, acc)
            tab, acc = annealing(np.reshape(np.argmax(model.predict(self.X_test[i:(i + 1)]), axis=-1), (self.rows, self.columns, 4)))
            acc2 = evaluate_2(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), tab)
            self.data.writeToFiles(tab, acc2, acc)
        print(colored('Fájlba írás befejeződött.', 'green'))

    def model_2(self):
        main_input = Input(shape=(self.rows * self.columns * 4, self.colours), name='main_input')
        if self.bidirectional is True:
            fn = Bidirectional
        else:
            fn = lambda x: x
        encoder = fn(LSTM(units=self.hidden_size, return_sequences=True, name="encoder"))(main_input)
        encoder = fn(LSTM(units=self.hidden_size, return_sequences=True))(encoder)
        encoder = fn(LSTM(units=self.hidden_size, return_sequences=True))(encoder)
        decoder = TimeDistributed(Dense(units=self.colours, activation='softmax'))(encoder)
        model = Model(inputs=main_input, outputs=decoder)
        model.compile(optimizer=Adam(clipnorm=1.),
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])

        hdf5 = 'model_weight.hdf5'

        if self.loadPrevModel is not False:
            print("Modell betöltése: " + self.loadPrevModel + ".h5")  
            try:                
                model.load_weights('../../saved_models/' + self.loadPrevModel + '.h5')
            except ValueError:
                print(colored('Nem megfelelő mentett modell fájlt választottál a megadott beállításokhoz.\nKérlek válassz másikat, vagy módosítsd a paramétereket a mentett modellhez.', 'red'))
                return
            except OSError:
                print(colored('Nincs ilyen nevű mentett fájl. Ellenőrizd a nevét, és a helyét.', 'red'))
                return
            model._make_train_function()
            with open('../../saved_models/' + self.loadPrevModel + '.pkl', 'rb') as f:
                weight_values = pickle.load(f)
            model.optimizer.set_weights(weight_values)

        else:
            print("Modell tanítása...")
            model.fit_generator(self.data.generator(batch_size=32, modelnum=self.modelnum), steps_per_epoch=100, epochs=self.nb_epochs, 
                                callbacks=[TerminateOnNaN(), 
                                ModelCheckpoint(hdf5, save_best_only=True), EarlyStopping(patience=self.earlyStopping)], 
                                validation_data=(self.X_val, self.Y_val))
            print(model.evaluate(self.X_test, self.Y_test, batch_size=32))

        if self.saveModel is True:
            model.save_weights('../../saved_models/' + str(self.rows) + 'x' + str(self.columns) + 'colours' + str(self.colours) + '.h5')
            symbolic_weights = getattr(model.optimizer, 'weights')
            weight_values = K.batch_get_value(symbolic_weights)
            with open('../../saved_models/' + str(self.rows) + 'x' + str(self.columns) + 'colours' + str(self.colours) + '.pkl', 'wb') as f:
                pickle.dump(weight_values, f)
            print('Modell mentése sikeresen befejeződött.')

        c = self.X_test.shape[0]
        self.data.writeArguments(int(c/100), self.rows, self.columns, self.colours)
        for i in range(0, c, 100):
            acc = evaluate(np.reshape(np.argmax(self.X_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)))
            acc2 = evaluate_2(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), np.reshape(np.argmax(self.X_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)))
            self.data.writeToFiles(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), 0, 0)
            self.data.writeToFiles(np.reshape(np.argmax(self.X_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), acc, acc2)
            acc = evaluate(np.reshape(np.argmax(model.predict(self.X_test[i:(i + 1)]), axis=-1), (self.rows, self.columns, 4)))
            acc2 = evaluate_2(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), np.reshape(np.argmax(model.predict(self.X_test[i:(i + 1)]), axis=-1), (self.rows, self.columns, 4)))
            self.data.writeToFiles(np.reshape(np.argmax(model.predict(self.X_test[i:(i + 1)]), axis=-1), (self.rows, self.columns, 4)), acc2, acc)
            X_tmp, acc2 = annealing_2(np.reshape(np.argmax(self.X_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), np.reshape(np.argmax(model.predict(self.X_test[i:(i + 1)]), axis=-1), (self.rows, self.columns, 4)), move_rotate=False)
            acc = evaluate(X_tmp)
            acc2 = evaluate_2(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), X_tmp)
            self.data.writeToFiles(X_tmp, acc2, acc)
            tab, acc = annealing(X_tmp, move_rotate=False)
            acc2 = evaluate_2(np.reshape(np.argmax(self.Y_test[i:(i + 1)], axis=-1), (self.rows, self.columns, 4)), tab)
            self.data.writeToFiles(tab, acc2, acc)
        print(colored('Fájlba írás befejeződött.', 'green'))

    def model_3(self):
        c = (self.X_test).shape[0]
        self.data.writeArguments(int(c/100), self.rows, self.columns, self.colours)
        for i in range(0, c, 100):
            acc = evaluate(self.X_test[i])
            acc2 = evaluate_2(self.Y_test[i], self.X_test[i])
            self.data.writeToFiles(self.Y_test[i], 0, 0)
            self.data.writeToFiles(self.X_test[i], acc, acc2)
            tab, acc = annealing(self.X_test[i], move_rotate=False)
            acc2 = evaluate_2(self.Y_test[i], tab)
            self.data.writeToFiles(tab, acc2, acc)
        print(colored('Fájlba írás befejeződött.', 'green'))

main = Run()
main.generateData()
main.learning()