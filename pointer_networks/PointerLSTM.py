from keras import initializers
from recurrent import _time_distributed_dense as time_distributed_dense
from keras.activations import tanh, softmax
from recurrent import LSTM
from keras.engine import InputSpec
import keras.backend as K


class PointerLSTM(LSTM):
    def __init__(self, units, gamma=1., *args, **kwargs):
        self.units = units
        self.gamma = gamma
        self.input_length = []
        super(PointerLSTM, self).__init__(units=units, *args, **kwargs)

    def build(self, input_shape):
        super(PointerLSTM, self).build(input_shape)
        self.input_spec = [InputSpec(shape=input_shape)]
        self.W1 = self.add_weight(shape=(self.units, 1),
                                  name='W1',
                                  initializer='orthogonal')
        self.W2 = self.add_weight(shape=(self.units, 1),
                                  name='W2',
                                  initializer='orthogonal')
        self.vt = self.add_weight(shape=(input_shape[1], 1),
                                  name='vt',
                                  initializer='orthogonal')
        self.built = True

    def call(self, x, mask=None, training=None, initial_state=None):
        input_shape = self.input_spec[0].shape
        en_seq = x
        x_input = x[:, input_shape[1]-1, :]
        x_input = K.repeat(x_input, input_shape[1])
        initial_states = self.get_initial_state(x_input)

        constants = self.get_constants(x_input, training=None)
        constants.append(en_seq)
        preprocessed_input = self.preprocess_input(x_input)

        last_output, outputs, states = K.rnn(self.step, preprocessed_input,
                                             initial_states,
                                             go_backwards=self.go_backwards,
                                             constants=constants,
                                             input_length=input_shape[1])

        return outputs

    def step(self, x_input, states, training=None):
        input_shape = self.input_spec[0].shape
        en_seq = states[-1]
        _, [h, c] = super(PointerLSTM, self).step(x_input, states[:-1])

        # vt*tanh(W1*e+W2*d)
        dec_seq = K.repeat(h, input_shape[1])
        Eij = time_distributed_dense(en_seq, self.W1, output_dim=1)
        Dij = time_distributed_dense(dec_seq, self.W2, output_dim=1)
        U = self.vt * tanh(Eij + Dij)
        U = K.squeeze(U, 2)

        # make probability tensor
        pointer = softmax(self.gamma * U)
        return pointer, [h, c]

    def compute_output_shape(self, input_shape):
        # output shape is not affected by the attention component
        return (input_shape[0], input_shape[1], input_shape[1])

class PointerLSTM2(LSTM):
    def __init__(self, units, gamma=1., *args, **kwargs):
        self.units = units
        self.gamma = gamma
        self.input_length = []
        super(PointerLSTM2, self).__init__(units=units, *args, **kwargs)

    def build(self, input_shape):
        super(PointerLSTM2, self).build(input_shape)
        self.states = [None, None, None]
        self.input_spec = [InputSpec(shape=input_shape)]
        self.W1 = self.add_weight(shape=(self.units, 1),
                                  name='W1',
                                  initializer='orthogonal')
        self.W2 = self.add_weight(shape=(self.units, 1),
                                  name='W2',
                                  initializer='orthogonal')
        self.vt = self.add_weight(shape=(input_shape[1], 1),
                                  name='vt',
                                  initializer='orthogonal')
        self.built = True
        
    def get_initial_state(self, inputs):
        input_shape = self.input_spec[0].shape
        # build an all-zero tensor of shape (samples, output_dim)
        initial_state = K.zeros_like(inputs)  # (samples, timesteps, input_dim)
        initial_state = K.sum(initial_state, axis=(1, 2))  # (samples,)
        _initial_state = K.expand_dims(initial_state)  # (samples, 1)
        initial_state = K.tile(_initial_state, [1, self.units])  # (samples, output_dim)
        initial_state = [K.tile(_initial_state, [1, input_shape[1]])] + [initial_state for _ in range(len(self.states[1:]))]
        return initial_state

    def call(self, x, mask=None, training=None, initial_state=None):
        input_shape = self.input_spec[0].shape
        en_seq = x
        x_input = x[:, input_shape[1]-1, :]
        x_input = K.repeat(x_input, input_shape[1])
        initial_states = self.get_initial_state(x_input)

        constants = self.get_constants(x_input, training=None)
        constants.append(en_seq)
        preprocessed_input = self.preprocess_input(x_input)

        last_output, outputs, states = K.rnn(self.step, preprocessed_input,
                                             initial_states,
                                             go_backwards=self.go_backwards,
                                             constants=constants,
                                             input_length=input_shape[1])

        return outputs

    def step(self, x_input, states, training=None):
        input_shape = self.input_spec[0].shape
        en_seq = states[-1]
        _, [h, c] = super(PointerLSTM2, self).step(x_input, states[1:-1])

        # vt*tanh(W1*e+W2*d)
        dec_seq = K.repeat(h, input_shape[1])
        Eij = time_distributed_dense(en_seq, self.W1, output_dim=1)
        Dij = time_distributed_dense(dec_seq, self.W2, output_dim=1)
        U = self.vt * tanh(Eij + Dij)
        U = K.squeeze(U, 2) # U folytonos érték az összes elemre - minél nagyobb annál jobb az azon az indexen szereplő permutáció elem
        # kidobom az U-ból azokat, akiket már korábban kiválasztottam - annak az U-ját kicserélni mínusz végtelenre

        # make probability tensor
        #U = U * K.cast(K.not_equal(pointer, K.max(states[0], axis=-1, keepdims=True)), K.floatx())
        pointer = softmax(self.gamma * U) # pointer : valószínűség, hogy melyik permutáció elemet mennyi valószínűséggel választom
        # 0 valószínűség azokra, akiket már kiválaszottunk
        prev_winners = states[0]
        pointer = pointer * (1 - prev_winners)
        pointer = pointer/K.sum(pointer, axis=-1, keepdims=True) # ezután a valószínűségek 1-re összegződnek
        #pointer = softmax(pointer)
        prev_winners = prev_winners + K.cast(K.equal(pointer, K.max(pointer, axis=-1, keepdims=True)), K.floatx())
        return pointer, [prev_winners, h, c]

    def compute_output_shape(self, input_shape):
        # output shape is not affected by the attention component
        return (input_shape[0], input_shape[1], input_shape[1])

