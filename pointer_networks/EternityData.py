#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from keras.utils.np_utils import to_categorical
from keras.utils import Sequence
from tetraAnnealing import *
from termcolor import colored
import sys

class EternityData():
    fileName = "empty"
    
    def __init__(self, rows, columns, colours):
        self.rows = rows
        self.columns = columns
        self.colours = colours
        
    def generateSolvedTables(self, count):
        solved_boards = -1 * np.ones((count, self.rows, self.columns, 4), dtype=np.int64)
        solved_board = -1 * np.ones((self.rows, self.columns, 4), dtype=np.int64)
        for c in range(count):
            for i in range(self.rows - 1):
                for j in range(self.columns):
                    color = np.random.randint(1, self.colours)
                    solved_board[i, j, 2] = color
                    solved_board[i + 1, j, 0] = color # two tiles next to each other
            for i in range(self.rows):
                for j in range(self.columns - 1):
                    color = np.random.randint(1, self.colours)
                    solved_board[i, j, 3] = color
                    solved_board[i, j + 1, 1] = color # two tiles under each other
            # frame must be 0
            for i in range(self.columns): # first and last row
                color = 0
                solved_board[0, i, 0] = color
                solved_board[self.rows - 1, i, 2] = color
            for j in range(self.rows): # left and right side
                color = 0
                solved_board[j, 0, 1] = color
                solved_board[j, self.columns - 1, 3] = color
            solved_boards[c, :, :, :] = solved_board.copy()
        return np.reshape(solved_boards, (count, self.rows * self.columns * 4, 1))

    def rotate(self, board, i, j, k):
        rotated_board = board.copy()
        rotated_board[i, j, :] = np.roll(board[i, j, :], k)
        return rotated_board

    def mixTables(self, boards, modelnum):
        permutations = np.repeat(np.arange(self.rows * self.columns)[None, :], boards.shape[0], axis=0)
        mixed_boards = -1 * np.ones_like(boards)
        for c in range(boards.shape[0]):
            board = np.reshape(boards[c], (self.rows, self.columns, 4))
            rp = np.random.permutation(self.rows * self.columns)
            permutations[c] = np.argsort(rp).copy()
            mixed_board = self.permutation(board, rp)
            mixed_boards[c] = np.reshape(mixed_board, (self.rows * self.columns * 4, 1)).copy()
        if modelnum == 0 or modelnum == 1 or modelnum == 2:
            return to_categorical(mixed_boards, self.colours), to_categorical(boards, self.colours)
        elif modelnum == 3:
            return np.reshape(mixed_boards, (boards.shape[0], self.rows, self.columns, 4)), np.reshape(boards, (boards.shape[0], self.rows, self.columns, 4))

    def generator(self, batch_size, modelnum):
        while True:
            yield self.mixTables(self.generateSolvedTables(batch_size), modelnum)

    def permutation(self, board, rp):
        mixed_board = -1 * np.ones_like(board)
        for i in range(self.rows):
            for j in range(self.columns):
                k = rp[i * self.columns + j]
                i2, j2 = k // self.columns, np.mod(k, self.columns)
                mixed_board[i2, j2, :] = board[i, j, :]
        return mixed_board
    
    def printTiles(self, board):
        n, m, _ = np.shape(board)
        for i in range(n):
            for j in range(m):
                print("--" + str(board[i, j, 0]) + "-- | ", end="")
            print()
            for j in range(m):
                print(str(board[i, j, 1]) + "---" + str(board[i, j, 3]) + " | ", end="")
            print()
            for j in range(m):
                print("--" + str(board[i, j, 2]) + "-- | ", end="")
            print()
            print("______  "* m)
            
    def setFileName(self, filename):
        global fileName
        fileName = filename

    def writeArguments(self, trainCount, rows, columns, colours):
        try:
            f = open('../../trained_tables/' + fileName, "a")
        except FileNotFoundError:
            print(colored('Fájlba írás nem sikerült, mivel nincs ilyen mappa: ../../trained_tables/', 'red'))
            sys.exit()
        f.write(str(trainCount) + ' ' + str(rows) + ' ' + str(columns) + ' ' + str(colours));
        f.write('\n')
        
    def writeToFiles(self, board, evaluate_1, evaluate_2):
        n, m, _ = np.shape(board)
        global f
        try:
            f = open('../../trained_tables/' + fileName, "a")
        except FileNotFoundError:
            print(colored('Fájlba írás nem sikerült, mivel nincs ilyen mappa: ../../trained_tables/', 'red'))
            sys.exit()
        for i in range(n):
            for j in range(m):
                f.write( str(board[i, j, 0]) + ' ' + str(board[i, j, 3]) + ' ' + str(board[i, j, 2]) + ' ' + str(board[i, j, 1]))
                f.write('\n')
        f.write(str(evaluate_1) + ' ' + str(evaluate_2))
        f.write('\n')