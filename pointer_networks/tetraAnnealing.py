#!/usr/bin/python
''' 
Implementation: Stelios Babrerakis, 
                 email: chefarov@gmail.com
                 copyleft May 2012
                 
'''          
import math
import random
import re
import numpy as np
from scipy import optimize

def evaluate(board):
    n, m, _ = np.shape(board)
    conflicts = 0
    for i in range(n - 1):
        for j in range(m):
            if board[i, j, 2] != board[i + 1, j, 0]: # egymás mellett (bal és jobb)
                conflicts = conflicts + 1
    for i in range(n):
        for j in range(m - 1):
            if board[i, j, 3] != board[i, j + 1, 1]: #egymás alatt (alsó és felső)
                conflicts = conflicts + 1
    for j in range (m):
        if board[0, j, 0] != 0: # first rows edges wrong
            conflicts = conflicts + 1
    for j in range (m):
        if board[n-1, j, 2] != 0: # last rows edges wrong
            conflicts = conflicts + 1
    for i in range (n):
        if board[i, 0, 1] != 0: # first column edges wrong
            conflicts = conflicts + 1
    for i in range (n):
        if board[i, m-1, 3] != 0: # last column edges wrong
            conflicts = conflicts + 1
    return conflicts


def evaluate_2(board_lstm, board):
    n, m, _ = np.shape(board)
    conflicts = 0
    for i in range(n):
        for j in range(m):
            for s in range(4):
                if board[i][j][s] != board_lstm[i][j][s]:
                    conflicts = conflicts + 1
    return conflicts

def swap(board, i1, j1, i2, j2):
    swapped_board = board.copy()
    swapped_board[i1, j1, :] = board[i2, j2, :]
    swapped_board[i2, j2, :] = board[i1, j1, :]
    return swapped_board

def rotate(board, i, j, k):
    rotated_board = board.copy()
    rotated_board[i, j, :] = np.roll(board[i, j, :], k)
    return rotated_board

def move(board, move_swap=True, move_rotate=True):
    n, m, _ = np.shape(board)
    i1, j1 = np.random.randint(n), np.random.randint(m)
    if move_swap is True and move_rotate is True:
        r = np.random.randint(2)
    elif move_swap is True:
        r = 0
    elif move_rotate is True:
        r = 1
    if r == 0:
        while True:
            i2, j2 = np.random.randint(n), np.random.randint(m)
            if i1 != i2 or j1 != j2:
                break
        moved_board = swap(board, i1, j1, i2, j2)
    else:
        moved_board = rotate(board, i1, j1, 1)
    return moved_board

def annealing_2(board, board_LSTM, move_swap=True, move_rotate=True, maxSteps=2500):
    best_board = board.copy()
    prev_board = board.copy()
    steps = 0
    temperature = 100.0
    finalTemp = 0.001 # proper to get final probability small enough (less than 0.001)
    e = evaluate_2(best_board, board_LSTM)
    temperature = (e * temperature + (1-e) * finalTemp)
    coolingFactor = float((finalTemp / temperature) ** (1.0 / float(maxSteps))) #normalizing temperature timeline to number of steps
    while steps < maxSteps:
        steps = steps + 1
        temperature = temperature * coolingFactor
        new_board = move(prev_board, move_swap, move_rotate)
        new_val = evaluate_2(new_board, board_LSTM) 
        if new_val == 0: # found a solution without conflicts
            return new_board, 0
        delta = new_val - evaluate_2(prev_board, board_LSTM) # finding DE of new state against current.
        if delta < 0: # If new value is better (smaller) than current one
            prev_board = new_board # then we make the move for sure
        else: # If new value is bigger (worse) than current one
            probability = math.exp(-delta / temperature) # get probability to make the move
            if probability > np.random.rand():
                prev_board = new_board # make the move
        if new_val < evaluate_2(best_board, board_LSTM): # If new value is better (smaller) than current one
            best_board = new_board # then we make the move for sure
    return best_board, evaluate_2(best_board, board_LSTM)

def annealing(board, move_swap=True, move_rotate=True, maxSteps=2500):
    best_board = board.copy()
    prev_board = board.copy()
    steps = 0
    temperature = 100.0
    finalTemp = 0.001 # proper to get final probability small enough (less than 0.001)
    e=evaluate(best_board)
    temperature = (e * temperature + (1-e) * finalTemp)
    coolingFactor = float((finalTemp / temperature) ** (1.0 / float(maxSteps))) #normalizing temperature timeline to number of steps
    while steps < maxSteps:
        steps = steps + 1
        temperature = temperature * coolingFactor
        T = max(1-evaluate(board), temperature)
        new_board = move(prev_board, move_swap, move_rotate)
        new_val = evaluate(new_board) 
        if new_val == 0: # found a solution without conflicts
            return new_board, 0
        delta = new_val - evaluate(prev_board) # finding DE of new state against current.
        if delta < 0: # If new value is better (smaller) than current one
            prev_board = new_board # then we make the move for sure
        else: # If new value is bigger (worse) than current one
            probability = math.exp(-delta / temperature) # get probability to make the move
            if probability > np.random.rand():
                prev_board = new_board # make the move
        if new_val < evaluate(best_board): # If new value is better (smaller) than current one
            best_board = new_board # then we make the move for sure
    return best_board, evaluate(best_board)
